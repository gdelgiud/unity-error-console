# Unity Error Console

## Description

A simple console that displays exceptions on-screen and allows you to read their stacktrace. Very handy for debug builds where Unity's log can't be 
easily read.

## Instructions

Simply create an empty GameObject somewhere in your scene and attach the **ErrorConsole** MonoBehaviour to it. See the **ExampleScene** included in 
the root of this project to see it in action.

## Screenshots

![Main screen](./Images/Screenshot01.png) ![Exception list screen](./Images/Screenshot02.png) ![Exception detail screen](./Images/Screenshot03.png)

## License

```
Copyright 2017 Gustavo Del Giudice
   
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
