﻿using System;
using System.Threading;
using UnityEngine;
using Random = UnityEngine.Random;

public class ExampleErrorGenerator : MonoBehaviour
{
    public float UpdateTime = 1.0f;
    public float ThrowProbability = 0.5f;
    
    private float _updateTimer;
    private string[] _messages;
    private Func<string, Exception>[] _exceptions;

    private void Awake()
    {
        _messages = new[]
        {
            "Some error message",
            "You've messed up",
            "This is a longer exception message",
            "Yet another message",
            "I've run out of ideas for exception messages here"
        };
        _exceptions = new Func<string, Exception>[]
        {
            message => new AbandonedMutexException(message),
            message => new ApplicationException(message),
            message => new NullReferenceException(message),
            message => new InvalidOperationException(message)
        };
    }
    
    private void Update()
    {
        _updateTimer += Time.deltaTime;
        if (_updateTimer > UpdateTime)
        {
            if (Random.value < ThrowProbability)
            {
                GenerateException();
            }
            _updateTimer = 0;
        }
    }
        
    public void GenerateException()
    {
        var messageIndex = Mathf.FloorToInt(Random.Range(0.0f, _messages.Length));
        var exceptionIndex = Mathf.FloorToInt(Random.Range(0.0f, _exceptions.Length));
        var message = _messages[messageIndex];
        throw _exceptions[exceptionIndex](message);
    }
}
