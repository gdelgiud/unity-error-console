﻿using Domain;
using UnityEngine;

namespace Emitter
{
    public class EventEmitter : IEventEmitter
    {
        private IEventListener _eventListener;
        
        public void SetEventListener(IEventListener eventListener)
        {
            if (eventListener == null)
                RemoveEventListener();
            else
                InternalSetEventListener(eventListener);
        }

        private void InternalSetEventListener(IEventListener eventListener)
        {
            _eventListener = eventListener;
            Application.logMessageReceived += OnLog;
        }

        public void RemoveEventListener()
        {
            Application.logMessageReceived -= OnLog;
        }

        private void OnLog(string condition, string stacktrace, LogType type)
        {
            if (type == LogType.Exception)
            {
                AddErrorEvent(condition, stacktrace);
            }
        }

        private void AddErrorEvent(string condition, string stacktrace)
        {
            if (_eventListener == null) return;
            var exceptionEvent = new ExceptionEvent(condition, stacktrace);
            _eventListener.OnExceptionEvent(exceptionEvent);
        }
    }
}
