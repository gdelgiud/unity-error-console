﻿namespace Emitter
{
    public interface IEventEmitter
    {
        void SetEventListener(IEventListener eventListener);
        void RemoveEventListener();
    }
}
