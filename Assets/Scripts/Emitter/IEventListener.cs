﻿using Domain;

namespace Emitter
{
    public interface IEventListener
    {
        void OnExceptionEvent(ExceptionEvent exceptionEvent);
    }
}
