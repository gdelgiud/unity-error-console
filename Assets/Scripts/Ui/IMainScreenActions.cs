﻿namespace Ui
{
    public interface IMainScreenActions
    {
        void OnErrorsButtonClicked();
        void OnClearButtonClicked();
    }
}
