﻿namespace Ui
{
    public interface IExceptionEventListActions
    {
        void OnItemSelected(int index);
        void OnCloseButtonClicked();
        void OnClearButtonClicked();
    }
}
