﻿namespace Ui
{
    public interface IScreens
    {
        IMainScreen GetMainScreen();
        IExceptionEventListScreen GetExceptionEventListScreen();
        IExceptionEventDetailScreen GetExceptionEventDetailScreen();

        void ShowMainScreen();
        void ShowExceptionEventListScreen();
        void ShowExceptionEventDetailScreen();
    }
}
