﻿namespace Ui
{
    public interface IMainScreen
    {
        void SetErrorsCount(int count);
        void FlashErrorButton(float duration);
        void ShowErrorsIndicator();
        void HideErrorsIndicator();
        void ShowClearButton();
        void HideClearButton();
        void SetActions(IMainScreenActions actions);
    }
}
