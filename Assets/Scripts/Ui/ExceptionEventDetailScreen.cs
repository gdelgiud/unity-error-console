﻿using Domain;
using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class ExceptionEventDetailScreen : MonoBehaviour, IExceptionEventDetailScreen
    {
        public Button CloseButton;
        public Text Title;
        public Text Stacktrace;

        private IExceptionEventDetailActions _actions;

        public void Start()
        {
            CloseButton.onClick.AddListener(OnCloseButtonClicked);
        }

        public void OnDestroy()
        {
            CloseButton.onClick.RemoveAllListeners();
        }

        public void SetExceptionEvent(ExceptionEvent exceptionEvent)
        {
            Title.text = exceptionEvent.Title;
            Stacktrace.text = exceptionEvent.Stacktrace;
        }

        public void SetActions(IExceptionEventDetailActions actions)
        {
            _actions = actions;
        }

        private void OnCloseButtonClicked()
        {
            if (_actions != null)
                _actions.OnCloseButtonClicked();
        }
    }
}
