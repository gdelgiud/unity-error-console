﻿using UnityEngine;

namespace Ui
{
    public class Screens : MonoBehaviour, IScreens
    {
        [SerializeField] private MainScreen _mainScreen;
        [SerializeField] private ExceptionEventListScreen _exceptionEventListScreen;
        [SerializeField] private ExceptionEventDetailScreen _exceptionEventDetailScreen;

        private MainScreen _mainScreenInstance;
        private ExceptionEventListScreen _exceptionEventListScreenInstance;
        private ExceptionEventDetailScreen _exceptionEventDetailScreenInstance;

        public void Awake()
        {
            _mainScreenInstance = InstantiateScreen(_mainScreen);
            _exceptionEventListScreenInstance = InstantiateScreen(_exceptionEventListScreen);
            _exceptionEventDetailScreenInstance = InstantiateScreen(_exceptionEventDetailScreen);
        }

        public IMainScreen GetMainScreen()
        {
            return _mainScreenInstance;
        }

        private T InstantiateScreen<T>(T screen) where T : Object
        {
            return Instantiate(screen, transform);
        }

        public IExceptionEventListScreen GetExceptionEventListScreen()
        {
            return _exceptionEventListScreenInstance;
        }

        public IExceptionEventDetailScreen GetExceptionEventDetailScreen()
        {
            return _exceptionEventDetailScreenInstance;
        }

        public void ShowMainScreen()
        {
            _mainScreenInstance.gameObject.SetActive(true);
            _exceptionEventListScreenInstance.gameObject.SetActive(false);
            _exceptionEventDetailScreenInstance.gameObject.SetActive(false);
        }

        public void ShowExceptionEventListScreen()
        {
            _mainScreenInstance.gameObject.SetActive(false);
            _exceptionEventListScreenInstance.gameObject.SetActive(true);
            _exceptionEventDetailScreenInstance.gameObject.SetActive(false);
        }

        public void ShowExceptionEventDetailScreen()
        {
            _mainScreenInstance.gameObject.SetActive(false);
            _exceptionEventListScreenInstance.gameObject.SetActive(false);
            _exceptionEventDetailScreenInstance.gameObject.SetActive(true);
        }
    }
}
