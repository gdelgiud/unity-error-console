﻿using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class ErrorCounterButton : MonoBehaviour
    {
        
        private const string TooManyErrorsText = "999+";
        
        public Button.ButtonClickedEvent onClick
        {
            get { return _button.onClick; }
        }
        
        [SerializeField] private Text _text;
        [SerializeField] private Color _flashColor;
        
        private Image _image;
        private Button _button;
        private float _flashDuration;
        private float _flashTimer;
        
        private void Awake()
        {
            _image = GetComponent<Image>();
            _button = GetComponent<Button>();
        }

        private void Update()
        {
            HandleFlash();
        }

        private void HandleFlash()
        {
            if (!IsFlashing()) return;
            
            _flashTimer -= Time.unscaledDeltaTime;
            var flashIntensity = _flashTimer / _flashDuration;
            SetTintIntensity(flashIntensity);
        }

        private void SetTintIntensity(float intensity)
        {
            intensity = Mathf.Clamp01(intensity);
            _image.color = Color.Lerp(Color.white, _flashColor, intensity);
        }

        private bool IsFlashing()
        {
            return _flashTimer > 0.0f;
        }

        public void SetErrorsCount(int count)
        {
            var text = GetErrorString(count);
            _text.text = text;
        }

        public void Flash(float duration)
        {
            _flashDuration = duration;
            _flashTimer = duration;
        }

        private static string GetErrorString(int count)
        {
            if (count > 999)
            {
                return TooManyErrorsText;
            }
            return count.ToString();
        }
    }
}
