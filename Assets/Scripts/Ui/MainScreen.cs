﻿using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class MainScreen : MonoBehaviour, IMainScreen
    {

        public ErrorCounterButton ErrorsButton;
        public Button ClearButton;

        private IMainScreenActions _actions;

        public void Start()
        {
            ErrorsButton.onClick.AddListener(OnErrorsButtonClicked);
            ClearButton.onClick.AddListener(OnClearButtonClicked);
        }

        private void OnErrorsButtonClicked()
        {
            if (_actions != null)
                _actions.OnErrorsButtonClicked();
        }

        private void OnClearButtonClicked()
        {
            if (_actions != null)
                _actions.OnClearButtonClicked();
        }

        public void SetErrorsCount(int count)
        {
            ErrorsButton.SetErrorsCount(count);
        }

        public void FlashErrorButton(float duration)
        {
            ErrorsButton.Flash(duration);
        }

        public void ShowErrorsIndicator()
        {
            ErrorsButton.gameObject.SetActive(true);
        }

        public void HideErrorsIndicator()
        {
            ErrorsButton.gameObject.SetActive(false);
        }

        public void ShowClearButton()
        {
            ClearButton.gameObject.SetActive(true);
        }

        public void HideClearButton()
        {
            ClearButton.gameObject.SetActive(false);
        }

        public void SetActions(IMainScreenActions actions)
        {
            _actions = actions;
        }
    }
}
