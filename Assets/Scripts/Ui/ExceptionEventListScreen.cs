﻿using System.Linq;
using Domain;
using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class ExceptionEventListScreen : MonoBehaviour, IExceptionEventListScreen
    {
        public ExceptionEventListItemView ListItemPrefab;
        public Button CloseButton;
        public Button ClearButton;
        public Transform Content;

        private IExceptionEventListActions _actions;

        public void Start()
        {
            CloseButton.onClick.AddListener(OnCloseButtonClicked);
            ClearButton.onClick.AddListener(OnClearButtonClicked);
        }

        private void OnCloseButtonClicked()
        {
            if (_actions != null)
                _actions.OnCloseButtonClicked();
        }

        private void OnClearButtonClicked()
        {
            if (_actions != null)
                _actions.OnClearButtonClicked();
        }

        public void AddElement(ExceptionEvent exceptionEvent)
        {
            var newView = Instantiate(ListItemPrefab, Content);
            newView.SetOnClickListener(() => OnListItemClicked(newView));
            newView.SetIndex(Content.childCount - 1);
            newView.SetTitle(exceptionEvent.Title);
        }

        public void RemoveAllElements()
        {
            var children = (from Transform child in Content select child.gameObject).ToList();
            children.ForEach(Destroy);

        }

        public void ShowClearButton()
        {
            ClearButton.gameObject.SetActive(true);
        }

        public void HideClearButton()
        {
            ClearButton.gameObject.SetActive(false);
        }

        public void SetActions(IExceptionEventListActions actions)
        {
            _actions = actions;
        }

        private void OnListItemClicked(ExceptionEventListItemView view)
        {
            if (_actions != null)
                _actions.OnItemSelected(view.GetIndex());
        }
    }
}
