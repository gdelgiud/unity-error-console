﻿using Domain;

namespace Ui
{
    public interface IExceptionEventListScreen
    {
        void AddElement(ExceptionEvent exceptionEvent);
        void RemoveAllElements();
        void ShowClearButton();
        void HideClearButton();
        void SetActions(IExceptionEventListActions actions);
    }
}
