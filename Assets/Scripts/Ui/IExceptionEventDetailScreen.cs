﻿using Domain;

namespace Ui
{
    public interface IExceptionEventDetailScreen
    {
        void SetExceptionEvent(ExceptionEvent exceptionEvent);
        void SetActions(IExceptionEventDetailActions actions);
    }
}
