﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Ui
{
    public class ExceptionEventListItemView : MonoBehaviour
    {
        public Text Title;
        
        private Button _button;
        private UnityAction _onClickAction;
        private int _index;

        public void Awake()
        {
            _button = GetComponent<Button>();
        }

        public void Start()
        {
            _button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            if (_onClickAction != null)
                _onClickAction();
        }

        public void OnDestroy()
        {
            _button.onClick.RemoveAllListeners();
        }

        public void SetTitle(string title)
        {
            Title.text = title;
        }

        public void SetOnClickListener(UnityAction action)
        {
            _onClickAction = action;
        }

        public void SetIndex(int index)
        {
            _index = index;
        }

        public int GetIndex()
        {
            return _index;
        }
    }
}
