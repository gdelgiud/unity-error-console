﻿using Domain;
using Emitter;
using Ui;
using UnityEngine;

namespace DefaultNamespace
{
    public class ErrorConsole : MonoBehaviour, IEventListener, IMainScreenActions, IExceptionEventListActions, IExceptionEventDetailActions
    {
        private const string CanvasPath = "Canvas";
        private const float FlashDuration = 0.25f;
        
        private IEventEmitter _eventEmitter;
        private IExceptionEventRepository _exceptionEventRepository;
        private Canvas _canvas;
        
        private IScreens _screens;
        private IMainScreen _mainScreen;
        private IExceptionEventListScreen _exceptionEventListScreen;
        private IExceptionEventDetailScreen _exceptionEventDetailScreen;

        public void Start()
        {
            CreateDependencies();
            InjectScreens();
            Bootstrap();
            ResetErrors();
        }

        public void OnDestroy()
        {
            _eventEmitter.RemoveEventListener();
        }

        private void CreateDependencies()
        {
            _eventEmitter = ProvideEventEmitter();
            _canvas = ProvideCanvas();
            _screens = ProvideScreens();
            _exceptionEventRepository = ProvideExceptionEventRepository();
        }

        private IEventEmitter ProvideEventEmitter()
        {
            var eventEmitter = new EventEmitter();
            return eventEmitter;
        }

        private Canvas ProvideCanvas()
        {
            var canvasAsset = Resources.Load<Canvas>(CanvasPath);
            var canvas = Instantiate(canvasAsset, transform);
            canvas.name = "ErrorConsoleCanvas";
            return canvas;
        }

        private IScreens ProvideScreens()
        {
            return _canvas.GetComponent<IScreens>();
        }

        private IExceptionEventRepository ProvideExceptionEventRepository()
        {
            return new ExceptionEventRepository();
        }

        private void InjectScreens()
        {
            _mainScreen = _screens.GetMainScreen();
            _exceptionEventListScreen = _screens.GetExceptionEventListScreen();
            _exceptionEventDetailScreen = _screens.GetExceptionEventDetailScreen();
        }

        private void Bootstrap()
        {
            _eventEmitter.SetEventListener(this);
            _mainScreen.SetActions(this);
            _exceptionEventListScreen.SetActions(this);
            _exceptionEventDetailScreen.SetActions(this);
            _screens.ShowMainScreen();
        }

        public void OnExceptionEvent(ExceptionEvent exceptionEvent)
        {
            _exceptionEventRepository.Store(exceptionEvent);
            var count = _exceptionEventRepository.GetAll().Count;
            _mainScreen.SetErrorsCount(count);
            _mainScreen.FlashErrorButton(FlashDuration);
            _mainScreen.ShowErrorsIndicator();
            _mainScreen.ShowClearButton();
            _exceptionEventListScreen.ShowClearButton();
            _exceptionEventListScreen.AddElement(exceptionEvent);
            
        }

        public void OnErrorsButtonClicked()
        {
            _screens.ShowExceptionEventListScreen();
        }

        public void OnClearButtonClicked()
        {
            ResetErrors();
        }

        private void ResetErrors()
        {
            _exceptionEventRepository.Clear();
            _mainScreen.SetErrorsCount(0);
            _mainScreen.HideErrorsIndicator();
            _mainScreen.HideClearButton();
            _exceptionEventListScreen.HideClearButton();
            _exceptionEventListScreen.RemoveAllElements();
        }

        public void OnItemSelected(int index)
        {
            _exceptionEventDetailScreen.SetExceptionEvent(_exceptionEventRepository.GetAll()[index]);
            _screens.ShowExceptionEventDetailScreen();
        }

        void IExceptionEventDetailActions.OnCloseButtonClicked()
        {
            _screens.ShowExceptionEventListScreen();
        }

        void IExceptionEventListActions.OnCloseButtonClicked()
        {
            _screens.ShowMainScreen();
        }
    }
}
