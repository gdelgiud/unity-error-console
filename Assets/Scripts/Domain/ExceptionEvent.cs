﻿namespace Domain
{
    public struct ExceptionEvent
    {
        public readonly string Title;
        public readonly string Stacktrace;

        public ExceptionEvent(string title, string stacktrace)
        {
            Title = title;
            Stacktrace = stacktrace;
        }
    }
}
