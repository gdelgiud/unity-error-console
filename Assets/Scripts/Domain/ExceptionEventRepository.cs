﻿using System.Collections.Generic;

namespace Domain
{
    public class ExceptionEventRepository : IExceptionEventRepository
    {
        private readonly List<ExceptionEvent> _events;
    
        public ExceptionEventRepository()
        {
            _events = new List<ExceptionEvent>();
        }
    
        public void Store(ExceptionEvent exceptionEvent)
        {
            _events.Add(exceptionEvent);
        }

        public List<ExceptionEvent> GetAll()
        {
            return _events;
        }

        public void Clear()
        {
            _events.Clear();
        }
    }
}
