﻿using System.Collections.Generic;

namespace Domain
{
    public interface IExceptionEventRepository
    {
        void Store(ExceptionEvent exceptionEvent);
        List<ExceptionEvent> GetAll();
        void Clear();
    }
}
